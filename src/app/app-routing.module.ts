import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SwitchLanguageComponent } from './switch-language/switch-language.component';

const routes: Routes = [
  {path:'language', component: SwitchLanguageComponent},
  {path:'**', 'redirectTo':'language'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
