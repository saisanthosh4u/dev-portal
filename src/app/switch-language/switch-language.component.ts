import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-switch-language',
  templateUrl: './switch-language.component.html',
  styleUrls: ['./switch-language.component.css']
})
export class SwitchLanguageComponent  {

  constructor(private translate: TranslateService) {
    translate.setDefaultLang('en');
  }
  
  useLanguage(language: string) {
    this.translate.use(language);
  }

}
